

package com.example.amir.businessofinterest;

        import android.content.Context;
        import android.graphics.Bitmap;
        import android.os.Bundle;
        import android.support.annotation.NonNull;
        import android.support.annotation.Nullable;
        import android.support.v4.app.Fragment;
        import android.text.TextUtils;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.ImageView;
        import android.widget.LinearLayout;
        import android.widget.Toast;

        import com.bumptech.glide.Glide;
        import com.bumptech.glide.request.RequestOptions;

        import net.glxn.qrgen.android.QRCode;

/**
 * Created by Amir on 12/1/2017.
 */

public class TestFragment extends Fragment {

    @Nullable
    @Override
    public Context getContext() {
        return super.getContext();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.barcode_scanner, container, false);
        ImageView barcodeBanner = root.findViewById(R.id.barcode_banner);

        Bitmap qrBitmap = generateQR(Config.APP_LINK+Config.APP_LINK_JOBID + "5a2726fee96bfb2b40563281" );
        barcodeBanner.setImageBitmap(qrBitmap);
        barcodeBanner.setScaleType(ImageView.ScaleType.FIT_XY);

        //qrImageView.requestLayout();
        return root;
    }

    private Bitmap generateQR(String text) {
        return QRCode.from(text).withSize(Utils.dpToPx(250), Utils.dpToPx(250)).bitmap();
    }
}

