package com.example.amir.businessofinterest;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Amir on 11/24/2017.
 */

public class SubCategoryItem implements Parcelable, Ideable {

    private String _id;
    private String categoryId;
    private String subCategoryName;
    private String subCategoryDescription;


    protected SubCategoryItem(Parcel in) {
        _id = in.readString();
        categoryId = in.readString();
        subCategoryName = in.readString();
        subCategoryDescription = in.readString();
    }

    public SubCategoryItem() {

    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(_id);
        dest.writeString(categoryId);
        dest.writeString(subCategoryName);
        dest.writeString(subCategoryDescription);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SubCategoryItem> CREATOR = new Creator<SubCategoryItem>() {
        @Override
        public SubCategoryItem createFromParcel(Parcel in) {
            return new SubCategoryItem(in);
        }

        @Override
        public SubCategoryItem[] newArray(int size) {
            return new SubCategoryItem[size];
        }
    };

    @Override
    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getSubCategoryName() {
        return subCategoryName;
    }

    public void setSubCategoryName(String subCategoryName) {
        this.subCategoryName = subCategoryName;
    }

    public String getSubCategoryDescription() {
        return subCategoryDescription;
    }

    public void setSubCategoryDescription(String subCategoryDescription) {
        this.subCategoryDescription = subCategoryDescription;
    }

    @Override
    public String toString() {
        return subCategoryName;
    }

    public static SubCategoryItem parseJSONObject(JSONObject json){
        SubCategoryItem subCategoryItem = new SubCategoryItem();
        try {
            subCategoryItem.set_id((String)json.get("_id"));
            subCategoryItem.setCategoryId((String)json.get("categoryId"));
            subCategoryItem.setSubCategoryDescription((String)json.get("subCategoryDescription"));
            subCategoryItem.setSubCategoryName((String)json.get("subCategoryName"));
            return subCategoryItem;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
