package com.example.amir.businessofinterest;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.amir.businessofinterest.Events.BgColorEvent;
import com.example.amir.businessofinterest.Events.FragmentDetach;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

/**
 * Created by Amir on 11/24/2017.
 */

public class AbstractTabFragment extends Fragment {
    private View root;

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void changeBGColor(BgColorEvent event) {
        root.setBackgroundColor(event.getColor());
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.tab1_fragment, container, false);
        //initViews(root);
        //bindData();
        //initListeners();
        return root;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        //tab1FragmentDelegate = (Tab1FragmentDelegate)context;
        if(!EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onDetach() {
        //tab1FragmentDelegate = null;
        if(EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        super.onDetach();
    }
}
