package com.example.amir.businessofinterest;

import android.app.Application;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by Amir on 11/24/2017.
 */

public class BusinessOfInterestApp extends Application {
    public static final String TAG = BusinessOfInterestApp.class.getSimpleName();
    private RequestQueue mRequestQueue;
    private static BusinessOfInterestApp mInstance;
    private static User login;

    public static User getLogin() {
        return login;
    }

    public static void setLogin(User login1) {
        login = login1;
    }


    public static synchronized BusinessOfInterestApp getInstance() {
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }
    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }


    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
    }
}
