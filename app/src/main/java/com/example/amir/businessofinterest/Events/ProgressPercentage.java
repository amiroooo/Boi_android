package com.example.amir.businessofinterest.Events;

/**
 * Created by Amir on 12/3/2017.
 */

public class ProgressPercentage {
    private Integer percentage;
    private Boolean showDefaultProgressBar;
    private Boolean visibility;

    public ProgressPercentage() {
        showDefaultProgressBar = false;
    }

    public ProgressPercentage(Boolean showDefaultProgressBar, Boolean isDefaultShow) {
        this.showDefaultProgressBar = showDefaultProgressBar;
        if (isDefaultShow){
            percentage = 1;
        }else{
            percentage = 100;
        }
    }




    public Integer getPercentage() {
        return percentage;
    }

    public void setPercentage(Integer percentage) {
        this.percentage = percentage;
    }

    public Boolean getShowDefaultProgressBar() {
        return showDefaultProgressBar;
    }

    public void setShowDefaultProgressBar(Boolean showDefaultProgressBar) {
        this.showDefaultProgressBar = showDefaultProgressBar;
    }

    public Boolean getVisibility() {
        if (percentage >= 99){
            return false;
        }
        return true;
    }

}
