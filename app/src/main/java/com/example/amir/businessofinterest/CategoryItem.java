package com.example.amir.businessofinterest;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Amir on 11/24/2017.
 */

public class CategoryItem implements Parcelable, Ideable {

    private String _id;
    private String categoryName;
    private String categoryDescription;
    private String pictureKey;

    public CategoryItem(String _id, String categoryName, String categoryDescription, String categoryPicture) {
        this._id = _id;
        this.categoryName = categoryName;
        this.categoryDescription = categoryDescription;
        this.pictureKey = categoryPicture;
    }

    protected CategoryItem(Parcel in) {
        _id = in.readString();
        categoryName = in.readString();
        categoryDescription = in.readString();
        pictureKey = in.readString();
    }

    public static final Creator<CategoryItem> CREATOR = new Creator<CategoryItem>() {
        @Override
        public CategoryItem createFromParcel(Parcel in) {
            return new CategoryItem(in);
        }

        @Override
        public CategoryItem[] newArray(int size) {
            return new CategoryItem[size];
        }
    };

    public String get_id() {
        return _id;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public String getCategoryDescription() {
        return categoryDescription;
    }

    public String getCategoryPictureUrl() {
        return Config.S3_BUCKET_URL + pictureKey;
    }
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this._id);
        parcel.writeString(this.categoryName);
        parcel.writeString(this.categoryDescription);
        parcel.writeString(this.pictureKey);
    }

    @Override
    public String toString() {
        return categoryName;
    }
}
