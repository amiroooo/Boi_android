package com.example.amir.businessofinterest.Events;

/**
 * Created by Amir on 12/1/2017.
 */

public class ImagePickerEvent {
    private String filepath;

    public ImagePickerEvent(String filepath) {
        this.filepath = filepath;
    }

    public String getFilepath() {
        return filepath;
    }
}
