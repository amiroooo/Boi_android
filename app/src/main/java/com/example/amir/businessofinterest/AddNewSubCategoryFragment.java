package com.example.amir.businessofinterest;

import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.thomashaertel.widget.MultiSpinner;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

/**
 * Created by Amir on 12/1/2017.
 */

public class AddNewSubCategoryFragment extends AdminAddFragment implements SubmitHandler{
    @Override
    void continueOnCreateView(LinearLayout layout) {
        addTextInputLayout("Sub Category Name","subCategoryName");
        addTextInputLayout("Sub Category Description","subCategoryDescription");
        Spinner categorySpinner = new Spinner(context);

        List<CategoryItem> categories = ((MainActivity)context).getCategoryList();
        ArrayAdapter<CategoryItem> adapter = new ArrayAdapter<CategoryItem>(context, R.layout.creation_spinner, categories);
        categorySpinner.setAdapter(adapter);
        categorySpinner.setSelection(0);
        addItem(categorySpinner, "categorySpinner");

        addSubmitButton("Create", this);

    }


    @Override
    public void onSubmit() {
        String subCategoryName = (String)getElementById("subCategoryName");
        String subCategoryDescription = (String)getElementById("subCategoryDescription");
        String categorySpinner = (String)getElementById("categorySpinner");


        if (TextUtils.isEmpty(subCategoryName)){
            Toast.makeText(context,"you must provide job name.",Toast.LENGTH_LONG).show();
            return;
        }
        if (TextUtils.isEmpty(categorySpinner)){
            Toast.makeText(context,"you must provide category.",Toast.LENGTH_LONG).show();
            return;
        }

        apiRequestHandler.getApiRequest().requestAddNewSubCategory(subCategoryName,subCategoryDescription,categorySpinner);
    }
}
