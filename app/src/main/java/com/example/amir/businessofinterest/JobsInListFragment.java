package com.example.amir.businessofinterest;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by Amir on 11/24/2017.
 */

public class JobsInListFragment extends Fragment {
    private static JobsInListFragment instance;
    private View root;
    private CategoryItem categoryItem;
    private TextView title;

    public static Fragment getInstance() {
        if (instance == null){
            instance = new JobsInListFragment();
        }
        return instance;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null){
            categoryItem = bundle.getParcelable("parcel_item");
        }
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.jobs_in_list, container, false);
        initViews(root);
        bindData();
        //initListeners();
        return root;
    }

    private void bindData() {
        title.setText(categoryItem.getCategoryName());
        final String[] colors = getContext().getResources().getStringArray(R.array.tablayout_colors);
        root.setBackgroundColor(Color.parseColor(colors[0]));
    }

    private void initViews(View root) {
        title = root.findViewById(R.id.category_name);
    }


    //TODO: establish recycleviewer and add items to it+viewholder etc..


}
