package com.example.amir.businessofinterest;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;

import com.thomashaertel.widget.MultiSpinner;

import org.json.JSONArray;

import java.util.ArrayList;

/**
 * Created by Amir on 12/2/2017.
 */

public class CustomMultiSpinner extends MultiSpinner {
    public CustomMultiSpinner(Context context) {
        super(context);
    }

    public CustomMultiSpinner(Context context, AttributeSet attr) {
        super(context, attr);
    }

    public CustomMultiSpinner(Context context, AttributeSet attr, int defStyle) {
        super(context, attr, defStyle);
    }

    public String getSelectedIds(){
        boolean[] selected = getSelected();
        StringBuilder arrString = new StringBuilder();
        int countAdded = 0;
        for (int i = 0; i < getSelected().length ; i++) {
            if (selected[i]) {
                if (countAdded > 0){
                    arrString.append(",");
                }
                arrString.append(((Ideable) getAdapter().getItem(i)).get_id());
                countAdded +=1;
            }
        }
        return arrString.toString();
    }
}
