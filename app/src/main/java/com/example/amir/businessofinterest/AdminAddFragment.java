package com.example.amir.businessofinterest;

import android.*;
import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.amir.businessofinterest.Events.BgColorEvent;
import com.example.amir.businessofinterest.Events.FragmentDetach;
import com.example.amir.businessofinterest.Events.ImagePickerEvent;
import com.example.amir.businessofinterest.Events.TabLayoutVisibility;
import com.mvc.imagepicker.ImagePicker;
import com.rilixtech.materialfancybutton.MaterialFancyButton;
import com.thomashaertel.widget.MultiSpinner;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;

/**
 * Created by Amir on 11/24/2017.
 */

public abstract class AdminAddFragment extends Fragment {
    protected View root;
    protected HashMap<String, Object> elements;
    protected Context context;
    protected APIRequestHandler apiRequestHandler;
    private String title;
    private LinearLayout layout;
    private String awaitingImageId = "";
    private LinearLayout.LayoutParams lp;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null){
            title = bundle.getString("title","");
        }
        lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, Utils.dpToPx(50));
        lp.setMargins(0,Utils.dpToPx(10),0,Utils.dpToPx(10));
        lp.gravity = Gravity.CENTER;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.admin_add_item, container, false);
        TextView titleView = root.findViewById(R.id.title);
        titleView.setText(title);
        elements = new HashMap<>();
        layout = (LinearLayout)root.findViewById(R.id.add_item_linear_layout);
        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        continueOnCreateView(layout);
        return root;
    }


    protected AutoCompleteTextView addTextInputLayout(String hint,String hashName){
        TextInputLayout view = new TextInputLayout(context);
        AutoCompleteTextView autoCompleteTextView = new AutoCompleteTextView(context);
        autoCompleteTextView.setHint(hint);
        view.addView(autoCompleteTextView);
        layout.addView(view);
        elements.put(hashName,autoCompleteTextView);
        return autoCompleteTextView;
    }

    protected void addUploadImageButton(String title, final String imagePickId){
        MaterialFancyButton facebookLoginBtn = new MaterialFancyButton(context);
        facebookLoginBtn.setText(title);
        facebookLoginBtn.setBackgroundColor(Color.parseColor("#3b5998"));
        facebookLoginBtn.setFocusBackgroundColor(Color.parseColor("#5474b8"));
        facebookLoginBtn.setTextSize(17);

        facebookLoginBtn.setIconFont("fontawesome.ttf");
        facebookLoginBtn.setIconResource("\uf1c5");
        facebookLoginBtn.setIconPosition(MaterialFancyButton.POSITION_LEFT);
        facebookLoginBtn.setFontIconSize(30);
        facebookLoginBtn.setRadius(30);
        facebookLoginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                awaitingImageId = imagePickId;
                handlePickImage();
            }
        });
        layout.addView(facebookLoginBtn,lp);
        elements.put(imagePickId,null);
    }


    private void handlePickImage(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if ((context.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) && (context.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)) {
                ImagePicker.pickImage((Activity)context, "Select your image:");
            } else {
                ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 999);
                ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 998);
            }
        }else{
            ImagePicker.pickImage((Activity)context, "Select your image:");
        }
    }






    protected void addSubmitButton(String title, final SubmitHandler handler){

        MaterialFancyButton facebookLoginBtn = new MaterialFancyButton(context);
        facebookLoginBtn.setText(title);
        facebookLoginBtn.setBackgroundColor(Color.parseColor("#3b5998"));
        facebookLoginBtn.setFocusBackgroundColor(Color.parseColor("#5474b8"));
        facebookLoginBtn.setTextSize(17);


        facebookLoginBtn.setIconFont("fontawesome.ttf");
        facebookLoginBtn.setIconResource("\uf067");
        facebookLoginBtn.setIconPosition(MaterialFancyButton.POSITION_LEFT);
        facebookLoginBtn.setFontIconSize(30);
        facebookLoginBtn.setRadius(30);

        facebookLoginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handler.onSubmit();
            }
        });
        layout.addView(facebookLoginBtn,lp);
    }



    protected void addItem(Object item, String hashName){
        if (item instanceof View) {
            RelativeLayout rl = new RelativeLayout(context);
            RelativeLayout.LayoutParams rlp = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            rlp.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
            //lp.height = Utils.dpToPx(20);
            rl.addView((View)item,rlp);
            layout.addView(rl,lp);
        }
        elements.put(hashName,item);
    }

    public void addItem(View item, String hashName, int i) {
        if (item instanceof View) {

            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, i);
            lp.setMargins(0,Utils.dpToPx(10),0,Utils.dpToPx(10));
            lp.gravity = Gravity.CENTER;
            RelativeLayout rl = new RelativeLayout(context);
            RelativeLayout.LayoutParams rlp = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            rlp.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
            //lp.height = Utils.dpToPx(20);
            rl.addView((View)item,rlp);
            layout.addView(rl,lp);
        }
        elements.put(hashName,item);
    }

    protected Object getElementById(String hashName){
        if (elements.get(hashName) == null){
            return null;
        }
        if (elements.get(hashName) instanceof AutoCompleteTextView){
            AutoCompleteTextView t = (AutoCompleteTextView)elements.get(hashName);
            return t.getText().toString();
        }
        if (elements.get(hashName) instanceof CustomMultiSpinner){
            CustomMultiSpinner t = (CustomMultiSpinner)elements.get(hashName);
            return (t.getSelectedIds());
        }
        if (elements.get(hashName) instanceof Spinner){
            Spinner t = (Spinner)elements.get(hashName);
            return ((Ideable)t.getSelectedItem()).get_id();
        }



        if (elements.get(hashName) instanceof String){
            return elements.get(hashName);
        }
        return elements.get(hashName);
    }

    @Override
    public void onAttach(Context context) {
        this.context = context;
        this.apiRequestHandler = (APIRequestHandler)context;
        super.onAttach(context);
        if(!EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onDetach() {

        if(EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().unregister(this);
        }
        EventBus.getDefault().post(new FragmentDetach());
        this.context = null;
        this.apiRequestHandler = null;
        super.onDetach();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onPickImage(ImagePickerEvent event){
        if (!TextUtils.isEmpty(awaitingImageId)){
            elements.put(awaitingImageId,event.getFilepath());
        }
    }

    abstract void continueOnCreateView(LinearLayout viewById);
}
