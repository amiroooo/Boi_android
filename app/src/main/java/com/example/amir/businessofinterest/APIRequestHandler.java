package com.example.amir.businessofinterest;

/**
 * Created by Amir on 12/1/2017.
 */

interface APIRequestHandler {
    ApiRequests getApiRequest();
}
