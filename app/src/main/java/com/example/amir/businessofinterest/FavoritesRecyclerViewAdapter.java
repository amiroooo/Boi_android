package com.example.amir.businessofinterest;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.example.amir.businessofinterest.Events.Tab1DataResponse;
import com.example.amir.businessofinterest.Events.Tab3DataResponse;
import com.rilixtech.materialfancybutton.MaterialFancyButton;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Amir on 11/24/2017.
 */

public class FavoritesRecyclerViewAdapter extends RecyclerView.Adapter<FavoritesRecyclerViewAdapter.JobViewHolder> {

        private List<JobItem> mData;
        private LayoutInflater mInflater;
        private ItemClickListener mClickListener;
        private Context context;

        // data is passed into the constructor
        FavoritesRecyclerViewAdapter(Context context) {
            this.context = context;
            this.mInflater = LayoutInflater.from(context);
            this.mData = new ArrayList<JobItem>();
        }

        // inflates the cell layout from xml when needed
        @Override
        public JobViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = mInflater.inflate(R.layout.job_favourite_viewholder, parent, false);
            return new JobViewHolder(view);
        }

        // binds the data to the textview in each cell
        @Override
        public void onBindViewHolder(JobViewHolder holder, int position) {
            JobItem categoryItem = mData.get(position);
            holder.updateJobViewHolderWithCategoryItem(categoryItem);
        }

        // total number of cells
        @Override
        public int getItemCount() {
            if (mData == null){
                return 0;
            }
            return mData.size();
        }

    public void updateData(Tab3DataResponse event) {
        mData = event.getData();
        notifyDataSetChanged();
    }


    // stores and recycles views as they are scrolled off screen
        public class JobViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            JobItem jobItem;
            TextView jobNameTextView;
            TextView jobDescriptionTextView;
            ImageView jobImageView;
            MaterialFancyButton favButton;
            JobViewHolder(View itemView) {
                super(itemView);

                itemView.setOnClickListener(this);
            }

            @Override
            public void onClick(View view) {
                if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
            }

            public void updateJobViewHolderWithCategoryItem(JobItem jobItem) {
                this.jobItem = jobItem;
                jobNameTextView = itemView.findViewById(R.id.job_title);
                jobDescriptionTextView = itemView.findViewById(R.id.job_description);
                jobImageView = itemView.findViewById(R.id.job_image);
                favButton = itemView.findViewById(R.id.fav_button);

                jobNameTextView.setText(jobItem.getJobName());
                jobDescriptionTextView.setText(jobItem.getJobDescription());

                RequestOptions myOptions = new RequestOptions().placeholder(R.mipmap.overlay).centerCrop().transform(new RoundedCorners(20));
                Glide.with(itemView.getContext())
                        .load(jobItem.getImageUrl())
                        .apply(myOptions)
                        .into(jobImageView);


            }
        }

        // convenience method for getting data at click position
        JobItem getItem(int index) {
            return mData.get(index);
        }

        // allows clicks events to be caught
        void setClickListener(ItemClickListener itemClickListener) {
            this.mClickListener = itemClickListener;
        }

        // parent activity will implement this method to respond to click events
        public interface ItemClickListener {
            void onItemClick(View view, int position);
        }



}
