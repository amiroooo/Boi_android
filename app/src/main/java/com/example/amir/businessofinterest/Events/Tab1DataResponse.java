package com.example.amir.businessofinterest.Events;

import com.example.amir.businessofinterest.CategoryItem;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Amir on 11/24/2017.
 */

public class Tab1DataResponse {

    private ArrayList<CategoryItem> data;
    private Integer status;

    public Tab1DataResponse(ArrayList<CategoryItem> data) {
        this.data = data;
    }

    public ArrayList<CategoryItem> getData() {

        return data;
    }

    public void setData(ArrayList<CategoryItem> data) {
        this.data = data;
    }

    public static Tab1DataResponse parseResponse(JSONArray response) {
        ArrayList<CategoryItem> data = new ArrayList<>();
        for (int i = 0; i < response.length(); i++) {
            String id;
            String categoryName;
            String categoryDescription;
            String categoryImage;
            try {
                JSONObject obj = (JSONObject)response.get(i);
                id = obj.getString("_id");
                categoryName = obj.getString("jobCategoryName");
                categoryDescription = obj.getString("jobDescription");
                categoryImage = obj.getString("imageUrl");
                data.add(new CategoryItem(id, categoryName, categoryDescription,categoryImage));
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        Tab1DataResponse res = new Tab1DataResponse(data);
        res.setStatus(0);
        return res;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Tab1DataResponse(Integer status) {
        this.status = status;
    }
}
