package com.example.amir.businessofinterest;

import java.util.ArrayList;

/**
 * Created by Amir on 12/2/2017.
 */

class SubCategoryListEvent {
    ArrayList<SubCategoryItem> arr;
    public SubCategoryListEvent(ArrayList<SubCategoryItem> arr) {
        this.arr = arr;
    }

    public ArrayList<SubCategoryItem> getArr() {
        return arr;
    }
}
