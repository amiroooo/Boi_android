package com.example.amir.businessofinterest;

import android.text.TextUtils;
import android.widget.LinearLayout;
import android.widget.Toast;

/**
 * Created by Amir on 12/1/2017.
 */

public class AddNewCategoryFragment extends AdminAddFragment implements SubmitHandler{

    @Override
    void continueOnCreateView(LinearLayout layout) {
        addTextInputLayout("Category Name","categoryName");
        addTextInputLayout("Category Description","categoryDescription");
        addUploadImageButton("Category Image","categoryItem");
        addSubmitButton("Create", this);

    }

    @Override
    public void onSubmit() {
        String categoryName = (String)getElementById("categoryName");
        String categoryDescription = (String)getElementById("categoryDescription");
        String categoryItem = (String)getElementById("categoryItem");
        if (TextUtils.isEmpty(categoryName)){
            Toast.makeText(context,"you must provide category name.",Toast.LENGTH_LONG).show();
            return;
        }
        if (TextUtils.isEmpty(categoryName)){
            Toast.makeText(context,"you must provide category name.",Toast.LENGTH_LONG).show();
            return;
        }
        apiRequestHandler.getApiRequest().requestAddNewJobCategory(categoryName,categoryDescription,categoryItem);
    }
}
