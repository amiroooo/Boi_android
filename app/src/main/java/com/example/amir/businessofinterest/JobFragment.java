package com.example.amir.businessofinterest;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by Amir on 12/22/2017.
 */

public class JobFragment extends Fragment {

    private static JobFragment instance;
    private JobItem jobItem;
    private View root;
    private TextView title;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null){
            jobItem = bundle.getParcelable("parcel_item");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.job_fragmet_layout, container, false);
        initViews(root);
        bindData();
        initListeners();
        return root;
    }



    private void initViews(View root) {
        title = root.findViewById(R.id.job_name);

    }

    private void bindData() {
        title.setText(jobItem.getJobName());
    }

    private void initListeners() {

    }

    public static Fragment getInstance() {
        if (instance == null){
            instance = new JobFragment();
        }
        return instance;
    }

}
