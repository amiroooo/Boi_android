package com.example.amir.businessofinterest.Events;

/**
 * Created by Amir on 12/22/2016.
 */
public class BgColorEvent {
    public int getOldBg() {
        return oldBg;
    }

    private final int oldBg;
    private final int color;

    public BgColorEvent(int color, int oldBg) {
        this.color = color;
        this.oldBg = oldBg;
    }

    public int getColor() {
        return color;
    }
}
