package com.example.amir.businessofinterest;

/**
 * Created by Amir on 11/27/2017.
 */

class User {

    private String _id;
    private String login;
    private String password;
    private String email;
    private String facebook_id;
    private String google_plus_id;
    private String name;
	private String role;

    private String token;

    public User() {

    }

    public String getRole() {
        return role;
    }
    public String get_id() {
        return _id;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }

    public String getFacebook_id() {
        return facebook_id;
    }

    public String getGoogle_plus_id() {
        return google_plus_id;
    }

    public User(String _id, String login, String email, String name) {
        this._id = _id;
        this.login = login;
        this.email = email;
        this.name = name;
    }

    public void setFacebook_id(String facebook_id) {
        this.facebook_id = facebook_id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }
	
    public void setRole(String role) {
        this.role = role;
    }
	
    public void setEmail(String email) {
        this.email = email;
    }

    public void setGoogle_plus_id(String google_plus_id) {
        this.google_plus_id = google_plus_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isUserLegal(){
        if (getName() != null && get_id() != null && getLogin() != null && getEmail() != null){
            return true;
        }
        return false;
    }

    public boolean isUserVefiried(){
        if (getToken() != null){
            return true;
        }
        return false;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getLoginType() {
        if (getFacebook_id() != null){
            return 2;
        }
        return 1;
    }
}
