package com.example.amir.businessofinterest;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.amir.businessofinterest.Events.BgColorEvent;
import com.example.amir.businessofinterest.Events.Tab1DataResponse;
import com.example.amir.businessofinterest.Events.Tab3DataResponse;
import com.example.amir.businessofinterest.Events.TabLayoutVisibility;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import xyz.cybersapien.recyclerele.RecyclerELEAdapter;

/**
 * Created by Amir on 11/24/2017.
 */

public class Tab3Fragment extends Fragment implements FavoritesRecyclerViewAdapter.ItemClickListener, SwipeRefreshLayout.OnRefreshListener{
    public static final String TAG = Tab3Fragment.class.getSimpleName();
    private View root;
    private RecyclerView recyclerView;
    private FavoritesRecyclerViewAdapter adapter;
    private MainActivity context;
    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerELEAdapter recyclerELEAdapter;

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void notifyChangeInRoundItem(Tab3DataResponse event) {
        switch (event.getStatus()){
            case 0:{
                adapter.updateData(event);
                recyclerELEAdapter.setCurrentView(RecyclerELEAdapter.VIEW_NORMAL);
                break;
            }
            case 1:{
                recyclerELEAdapter.setCurrentView(RecyclerELEAdapter.VIEW_EMPTY);
                break;
            }
            case 2:{
                recyclerELEAdapter.setCurrentView(RecyclerELEAdapter.VIEW_ERROR);
                break;
            }
            case 3:{
                recyclerELEAdapter.setCurrentView(RecyclerELEAdapter.VIEW_LOADING);
                break;
            }
        }
        swipeRefreshLayout.setRefreshing(false);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void changeBGColor(BgColorEvent event) {
        root.setBackgroundColor(event.getColor());
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null){
            Object exampleClass = bundle.getParcelable("example_name");

            if (exampleClass != null) {
                //do smth if not null
            }

            //Boolean isPerma = bundle.getBoolean("isPerma");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.tab1_fragment, container, false);
        initViews(root);
        bindData();
        initListeners();
        swipeRefreshLayout = root.findViewById(R.id.swipe_refresh);
        swipeRefreshLayout.setOnRefreshListener(this);
        if (context != null && BusinessOfInterestApp.getLogin() != null && BusinessOfInterestApp.getLogin().isUserVefiried()) {
            context.getApiRequests().requestGetFavorites(BusinessOfInterestApp.getLogin().get_id());
        }
        return root;
    }

    // to define elements from view that you might want to use
    private void initViews(View v) {
        /* example
        leftTopPoints = (TextView)v.findViewById(R.id.label);
        backButton = (Button)v.findViewById(R.id.back_button);
        */
        recyclerView = (RecyclerView) v.findViewById(R.id.categories_recycle_view);

    }

    // to change some aspects of the view by changings some attributes to elements in frag_layout
    private void bindData(){
        /*
        leftTopPoints.setText("hello world");
        */
        LinearLayoutManager llm = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(llm);

        adapter = new FavoritesRecyclerViewAdapter(getContext());
        adapter.setClickListener(this);

        View loadingView = getLayoutInflater().inflate(R.layout.view_loading, recyclerView, false);
        View emptyView = getLayoutInflater().inflate(R.layout.view_empty, recyclerView, false);
        View errorView = getLayoutInflater().inflate(R.layout.view_error, recyclerView, false);
        recyclerELEAdapter = new RecyclerELEAdapter(adapter, emptyView, loadingView, errorView);
        recyclerView.setAdapter(recyclerELEAdapter);
    }



    private void initListeners() {
        /* example:
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        */
        recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy>0){
                    EventBus.getDefault().post(new TabLayoutVisibility(false));
                }else{
                    EventBus.getDefault().post(new TabLayoutVisibility(true));
                }
            }
        });

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = (MainActivity)context;
        if(!EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onDetach() {
        context = null;
        if(EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().unregister(this);
        }
        super.onDetach();
    }

    @Override
    public void onItemClick(View view, int position) {
        Log.i(TAG, "You clicked number " + adapter.getItem(position) + ", which is at cell position " + position);
        //Fragment jobsInListFragment = JobsInListFragment.getInstance();
        //Bundle bundle = new Bundle();
        //bundle.putParcelable("parcel_item",adapter.getItem(position));
        //jobsInListFragment.setArguments(bundle);
        //EventBus.getDefault().post(jobsInListFragment);
    }

    @Override
    public void onRefresh() {
        if (context != null && BusinessOfInterestApp.getLogin() != null && BusinessOfInterestApp.getLogin().isUserVefiried()) {
            context.getApiRequests().requestGetFavorites(BusinessOfInterestApp.getLogin().get_id());
        }
    }
}
