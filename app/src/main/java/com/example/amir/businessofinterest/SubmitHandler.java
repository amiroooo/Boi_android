package com.example.amir.businessofinterest;

/**
 * Created by Amir on 12/1/2017.
 */

interface SubmitHandler {
    void onSubmit();
}
