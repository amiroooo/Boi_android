package com.example.amir.businessofinterest.Events;

/**
 * Created by Amir on 12/1/2017.
 */

public class TabLayoutVisibility {
    Boolean shouldShow = true;

    public TabLayoutVisibility(Boolean shouldShow) {
        this.shouldShow = shouldShow;
    }

    public Boolean getShouldShow() {
        return shouldShow;
    }

    public void setShouldShow(Boolean shouldShow) {
        this.shouldShow = shouldShow;
    }
}
