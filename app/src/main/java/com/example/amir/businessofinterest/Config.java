package com.example.amir.businessofinterest;

/**
 * Created by Amir on 11/23/2017.
 */

public final class Config {

    public static final String GET_JOB_DETAILS = "/getjobdetails/";
    public static final String GET_CATEGORIES = "/getjobcategories";
    public static final String GET_JOBS_IN_CATEGORY = "/getjobsincategory/";
    public static final String ADD_JOB_CATEGORY = "/addjobcategory";
    public static final String S3_BUCKET_URL = "http://s3-us-west-2.amazonaws.com/elasticbeanstalk-us-west-2-206359030210/";
    public static final String ADD_FACEBOOK_USER_IF_NOT_EXIST = "/addfacebookuser";
    public static final String LOGIN = "/login";
    public static final String LOGIN_BY_APP = "/refreshtoken";
    public static final String SIGNUP = "/signup";
    public static final String ADD_GOOGLE_USER_IF_NOT_EXIST = "/addgoogleuser";
    public static final String REQUEST_GET_SUB_CATEGORIES = "/getsubcategories";
    public static final String REQUEST_ADD_SUB_CATEGORIES = "/addsubcategory";
    public static final String ADD_NEW_JOB = "/addnewjob";
    public static final String ADD_FAVORITE = "/addfavouritejob";
    public static final String GET_FAVORITES = "/getfavorites";
    public static final String ADD_NOTIFICATION = "/addnotification";
    public static final String GET_NOTIFICATION_PAGE = "/getjobnotifications/";
    public static final String APP_LINK = "businessofinterest.com";
    public static final String APP_LINK_JOBID = "/jobid/";

    public static String SERVER_URL = "http://default-environment.9cpuixfwcp.us-west-2.elasticbeanstalk.com";
    //private static final String LOCAL_URL = "http://localhost:8081";
    private static final String LOCAL_URL = "http://10.0.0.2:443";

    public static void changeUrlToLocal() {
        SERVER_URL = LOCAL_URL;
    }
}
