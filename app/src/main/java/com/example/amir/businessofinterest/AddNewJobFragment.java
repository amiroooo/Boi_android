package com.example.amir.businessofinterest;

import android.*;
import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.DragEvent;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.PointerIcon;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.amir.businessofinterest.Events.ImagePickerEvent;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.thomashaertel.widget.MultiSpinner;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

/**
 * Created by Amir on 12/1/2017.
 */

public class AddNewJobFragment extends AdminAddFragment implements SubmitHandler, OnMapReadyCallback, GoogleMap.OnMapClickListener, GoogleMap.OnMyLocationButtonClickListener, GoogleMap.OnMyLocationClickListener {
    private CustomMultiSpinner subCategorySpinner;
    private FrameLayout fl;
    private SupportMapFragment mapFragment;
    private GoogleMap mMap;
    private Marker marker;
    private AutoCompleteTextView locationEditBox;
    private int MY_LOCATION_REQUEST_CODE = 555;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    void continueOnCreateView(LinearLayout layout) {
        addTextInputLayout("Job Name", "jobName");
        addTextInputLayout("Job Description", "jobDescription");
        Spinner categorySpinner = new Spinner(context, Spinner.MODE_DIALOG);

        List<CategoryItem> categories = ((MainActivity) context).getCategoryList();
        ArrayAdapter<CategoryItem> adapter = new ArrayAdapter<CategoryItem>(context, R.layout.creation_spinner, categories);
        adapter.setDropDownViewResource(R.layout.creation_spinner);
        categorySpinner.setAdapter(adapter);
        categorySpinner.setPrompt("Choose Category");

        //categorySpinner.setDropDownHorizontalOffset(20);

        //categorySpinner.setSelection(0);

        categorySpinner.setGravity(Gravity.CENTER);
        addItem(categorySpinner, "categorySpinner");

        subCategorySpinner = new CustomMultiSpinner(context);
        subCategorySpinner.setHint("Choose Sub Category");
        subCategorySpinner.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
        subCategorySpinner.setGravity(Gravity.CENTER);

        subCategorySpinner.setBackgroundColor(getResources().getColor(R.color.white));
        addItem(subCategorySpinner, "subCategorySpinner");


        categorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                CategoryItem category = (CategoryItem) parent.getItemAtPosition(position);
                apiRequestHandler.getApiRequest().requestGetSubCategories(category.get_id());
            }

            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        //JobCategoriesSpinner to pick from
        //JobSubCategoriesSpinner to pick from
        addUploadImageButton("Job Thumbnail", "jobImage");


        locationEditBox = addTextInputLayout("Job Location", "jobLocation");


        FrameLayout f0 = new FrameLayout(context);
        int id = View.generateViewId();
        f0.setId(id);



        fl = new FrameLayout(context);
        id = View.generateViewId();
        fl.setId(id);
        f0.addView(fl);


        LinearLayout.LayoutParams lpp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        ImageView img = new ImageView(context);




        f0.addView(img,lpp);


        //fl.setLayerType(View.LAYER_TYPE_SOFTWARE, null);

        addItem(f0, "frameView", Utils.dpToPx(300));


        mapFragment = SupportMapFragment.newInstance();
        FragmentTransaction tr = getFragmentManager().beginTransaction();
        tr.replace(fl.getId(), mapFragment, "googleMap");
        tr.commit();
        mapFragment.getMapAsync(this);



        addSubmitButton("Create", this);

        final ScrollView scrollView = (ScrollView)root.findViewById(R.id.scroll_layout);
        scrollView.requestDisallowInterceptTouchEvent(true);



        img.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        // Disallow ScrollView to intercept touch events.
                        scrollView.requestDisallowInterceptTouchEvent(true);
                        // Disable touch on transparent view
                        return false;

                    case MotionEvent.ACTION_UP:
                        // Allow ScrollView to intercept touch events.
                        scrollView.requestDisallowInterceptTouchEvent(false);
                        return true;

                    case MotionEvent.ACTION_MOVE:
                        scrollView.requestDisallowInterceptTouchEvent(true);
                        return false;

                    default:
                        return true;
                }
            }
        });
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onSubCategoryListEvent(SubCategoryListEvent event) {
        //Toast.makeText(context,"sub category list Reached" + event.getArr().size(),Toast.LENGTH_LONG).show();

        ArrayAdapter<SubCategoryItem> adapter = new ArrayAdapter<SubCategoryItem>(context, R.layout.creation_spinner, event.getArr());
        subCategorySpinner.setAdapter(adapter, false, onSelectedListener);
    }


    private MultiSpinner.MultiSpinnerListener onSelectedListener = new MultiSpinner.MultiSpinnerListener() {
        public void onItemsSelected(boolean[] selected) {

            // Do something here with the selected items
        }
    };

    @Override
    public void onSubmit() {
        String jobName = (String) getElementById("jobName");
        String jobDescription = (String) getElementById("jobDescription");
        String jobImage = (String) getElementById("jobImage");
        String categorySpinner = (String) getElementById("categorySpinner");
        String subCategorySpinner = (String) getElementById("subCategorySpinner");
        String locationName = locationEditBox.getText().toString();


        if (TextUtils.isEmpty(jobName)) {
            Toast.makeText(context, "You must provide job name.", Toast.LENGTH_LONG).show();
            return;
        }
        if (TextUtils.isEmpty(jobName)) {
            Toast.makeText(context, "You must provide job name.", Toast.LENGTH_LONG).show();
            return;
        }
        if (TextUtils.isEmpty(categorySpinner)) {
            Toast.makeText(context, "You must provide category.", Toast.LENGTH_LONG).show();
            return;
        }

        if (TextUtils.isEmpty(subCategorySpinner)) {
            Toast.makeText(context, "You must provide subCategory.", Toast.LENGTH_LONG).show();
            return;
        }
        if (jobImage == null) {
            Toast.makeText(context, "You must provide Image.", Toast.LENGTH_LONG).show();
            return;
        }

        if (marker == null) {
            Toast.makeText(context, "You must provide Job Location.", Toast.LENGTH_LONG).show();
            return;
        }
        if (TextUtils.isEmpty(locationName)) {
            Toast.makeText(context, "You must provide Job Location Address.", Toast.LENGTH_LONG).show();
            return;
        }

        if (BusinessOfInterestApp.getLogin() == null) {
            Toast.makeText(context, "You must be logged in to submit new job.", Toast.LENGTH_LONG).show();
            return;
        }


        String userId = BusinessOfInterestApp.getLogin().get_id();
        String longitude = ((Double) marker.getPosition().longitude).toString();
        String latitude = ((Double) marker.getPosition().latitude).toString();

        apiRequestHandler.getApiRequest().requestAddNewJob(jobName, jobDescription, categorySpinner, subCategorySpinner, jobImage, longitude, latitude, locationName, userId);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.d("Amir", "onMapReady: ");
        mMap = googleMap;
        mMap.getUiSettings().setMyLocationButtonEnabled(true);
        mMap.getUiSettings().setZoomControlsEnabled(true);
        googleMap.setOnMapClickListener(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (context.checkSelfPermission(android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                mMap.setMyLocationEnabled(true);
            } else {
                ActivityCompat.requestPermissions((Activity) context, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, MY_LOCATION_REQUEST_CODE);

            }
        }

        mMap.setOnMyLocationButtonClickListener(this);
        mMap.setOnMyLocationClickListener(this);


    }











    @Override
    public void onMapClick(LatLng latLng) {
        MarkerOptions options = new MarkerOptions().snippet("Job Location")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))
                .anchor(0.5f, 0.5f).draggable(true);
        options.position(latLng);
        if (marker != null) {
            marker.remove();
        }

        marker = mMap.addMarker(options);
        locationEditBox.setText(getAddress(latLng.latitude, latLng.longitude));
    }

    public String getAddress(double lat, double lng) {
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
            if (addresses != null && addresses.size()>0) {
                Address obj = addresses.get(0);

                String add = obj.getAddressLine(0);
                /*
                add = add + "\n" + obj.getCountryName();
                add = add + "\n" + obj.getCountryCode();
                add = add + "\n" + obj.getAdminArea();
                add = add + "\n" + obj.getPostalCode();
                add = add + "\n" + obj.getSubAdminArea();
                add = add + "\n" + obj.getLocality();
                add = add + "\n" + obj.getSubThoroughfare();
                */
                return add;
            }else
                return null;
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
            return null;
        }
    }

    //TODO connect it with main activity to read accept permission in order o reload map.
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == MY_LOCATION_REQUEST_CODE) {
            if (ActivityCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions((Activity) context, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, MY_LOCATION_REQUEST_CODE);

            }else{
                mMap.setMyLocationEnabled(true);
                mMap.getUiSettings().setMyLocationButtonEnabled(true);
            }

        }
    }

    @Override
    public boolean onMyLocationButtonClick() {
        return false;
    }

    @Override
    public void onMyLocationClick(@NonNull Location location) {

    }
}
