package com.example.amir.businessofinterest.Events;

import com.example.amir.businessofinterest.CategoryItem;
import com.example.amir.businessofinterest.JobItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Amir on 11/24/2017.
 */

public class Tab3DataResponse {

    private ArrayList<JobItem> data;
    private Integer status;

    public Tab3DataResponse(ArrayList<JobItem> data) {
        this.data = data;
    }

    public ArrayList<JobItem> getData() {

        return data;
    }

    public void setData(ArrayList<JobItem> data) {
        this.data = data;
    }

    public static Tab3DataResponse parseResponse(JSONArray response) {
        ArrayList<JobItem> data = new ArrayList<>();
        for (int i = 0; i < response.length(); i++) {
            String id;
            JSONObject job;
            String jobLocationName;
            String jobDescription;
            String imageUrl;
            String ownerId;
            String categoryId;
            String jobName;
            try {
                JSONObject obj = (JSONObject)response.get(i);
                job = obj.getJSONObject("jobId");
                id = job.getString("_id");
                ownerId = job.getString("ownerId");
                categoryId = job.getString("categoryId");
                jobName = job.getString("jobName");
                JobItem cjob = new JobItem(id, ownerId, categoryId, jobName);
                if (job.has("jobDescription")) {
                    jobDescription = job.getString("jobDescription");
                    cjob.setJobDescription(jobDescription);
                }
                if (job.has("jobLocationName")) {
                    jobLocationName = job.getString("jobLocationName");
                    cjob.setJobAddress(jobLocationName);
                }
                if (job.has("imageUrl")) {
                    imageUrl = job.getString("imageUrl");
                    cjob.setImageUrl(imageUrl);
                }


                data.add(cjob);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        Tab3DataResponse res = new Tab3DataResponse(data);
        res.setStatus(0);
        return res;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Tab3DataResponse(Integer status) {
        this.status = status;
    }
}
