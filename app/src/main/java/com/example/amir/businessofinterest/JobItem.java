package com.example.amir.businessofinterest;

import android.graphics.Point;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Amir on 12/3/2017.
 */

public class JobItem implements Parcelable, Ideable {
    String _id;
    String ownerId;
    String categoryId;
    ArrayList<String> subCategoryIds;
    String jobName;
    String jobDescription;
    String jobAddress;
    String imageUrl;
    LatLng cords;
    String jobLocationName;

    public JobItem(String _id, String ownerId, String categoryId, String jobName) {
        this._id = _id;
        this.ownerId = ownerId;
        this.categoryId = categoryId;
        this.jobName = jobName;
    }

    protected JobItem(Parcel in) {
        _id = in.readString();
        ownerId = in.readString();
        categoryId = in.readString();
        subCategoryIds = in.createStringArrayList();
        jobName = in.readString();
        jobDescription = in.readString();
        jobAddress = in.readString();
        imageUrl = in.readString();
        cords = in.readParcelable(LatLng.class.getClassLoader());
        jobLocationName = in.readString();
    }

    public static final Creator<JobItem> CREATOR = new Creator<JobItem>() {
        @Override
        public JobItem createFromParcel(Parcel in) {
            return new JobItem(in);
        }

        @Override
        public JobItem[] newArray(int size) {
            return new JobItem[size];
        }
    };

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public ArrayList<String> getSubCategoryIds() {
        return subCategoryIds;
    }

    public void setSubCategoryIds(ArrayList<String> subCategoryIds) {
        this.subCategoryIds = subCategoryIds;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getJobDescription() {
        return jobDescription;
    }

    public void setJobDescription(String jobDescription) {
        this.jobDescription = jobDescription;
    }

    public String getJobAddress() {
        return jobAddress;
    }

    public void setJobAddress(String jobAddress) {
        this.jobAddress = jobAddress;
    }

    public String getImageUrl() {
        return Config.S3_BUCKET_URL + imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public LatLng getCords() {
        return cords;
    }

    public void setCords(LatLng cords) {
        this.cords = cords;
    }

    @Override
    public int describeContents() {
        return 0;
    }


    public String getJobLocationName() {
        return jobLocationName;
    }

    public void setJobLocationName(String jobLocationName) {
        this.jobLocationName = jobLocationName;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(_id);
        parcel.writeString(ownerId);
        parcel.writeString(categoryId);
        parcel.writeStringList(subCategoryIds);
        parcel.writeString(jobName);
        parcel.writeString(jobDescription);
        parcel.writeString(jobAddress);
        parcel.writeString(imageUrl);
        parcel.writeParcelable(cords, i);
        parcel.writeString(jobLocationName);
    }

    public static JobItem parseByJsonObject(JSONObject response) {
        if (!response.has("_id"))
            return null;
        if (!response.has("ownerId"))
            return null;
        if (!response.has("categoryId"))
            return null;
        if (!response.has("jobName"))
            return null;

        try {
            JobItem jobItem = new JobItem(response.getString("_id"), response.getString("ownerId"), response.getString("categoryId"), response.getString("jobName"));
            if (response.has("subCategoryId"))
                jobItem.setCategoryId(response.getString("subCategoryId"));
            if (response.has("jobDescription"))
                jobItem.setJobDescription(response.getString("jobDescription"));
            if (response.has("jobLocationName"))
                jobItem.setJobLocationName(response.getString("jobLocationName"));
            if (response.has("imageUrl"))
                jobItem.setImageUrl(response.getString("imageUrl"));
            if (response.has("geo")){
                JSONArray coord = new JSONArray(response.getString("geo"));
                LatLng cords = new LatLng(coord.getDouble(0),coord.getDouble(1));
                jobItem.setCords(cords);
            }

            return jobItem;
        } catch (JSONException e) {

        }
        return null;
    }
}
