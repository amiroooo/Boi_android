package com.example.amir.businessofinterest;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;

import com.android.volley.RetryPolicy;
import com.android.volley.VolleyLog;

import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonArrayRequest;
import com.android.volley.request.JsonObjectRequest;
import com.android.volley.request.SimpleMultiPartRequest;
import com.example.amir.businessofinterest.Events.ProgressPercentage;
import com.example.amir.businessofinterest.Events.Tab1DataResponse;
import com.example.amir.businessofinterest.Events.Tab3DataResponse;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Amir on 11/24/2017.
 */

public class ApiRequests {
    public static final String TAG = ApiRequests.class.getSimpleName();
    private Context context;

    public ApiRequests(Context context) {
        this.context = context;
    }

    //the only combination that is not available is, sending JsonObject, but recieving response JsonArray

    // this is how to build POST request, and add params to it
    public void requestJobsByCategory(String jobId) {
        String requestTag = "RequestJobsByCategory";
        String url = Config.SERVER_URL + Config.GET_JOBS_IN_CATEGORY;
        JSONArray params = new JSONArray();
        params.put(jobId);
        JsonArrayRequest stringRequest = new JsonArrayRequest(Request.Method.POST, url, params, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                // recieved response as json array response, now we can send it to its list/place of use
            }
        }, new APIErrorListener());
        BusinessOfInterestApp.getInstance().addToRequestQueue(stringRequest, TAG);
    }

    // This is how to build GET request, in this case you may need to add queries to the url., query example: "?=getall=true&cat=3&time=65"
    public void requestGetCategories() {
        String requestTag = "RequestGetCategories";
        String url = Config.SERVER_URL + Config.GET_CATEGORIES;

        JsonArrayRequest jsonObjReq = new JsonArrayRequest(url,
                new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {
                        // recieved response as json array response, now we can send it to its list/place of use
                        Tab1DataResponse data = Tab1DataResponse.parseResponse(response);
                        if (data.getData().isEmpty()){
                            data.setStatus(1);
                        }
                        EventBus.getDefault().post(data);
                    }
                }, new APIErrorListener(){
                        @Override
                        public void addErrorHandler(){

                            EventBus.getDefault().post(new Tab1DataResponse(2));
                        }
        });
        jsonObjReq.setShouldCache(false);
        jsonObjReq.setRetryPolicy(new ApiRetryPolicy(3000,1,1f,jsonObjReq));
        EventBus.getDefault().post(new Tab1DataResponse(3));
        BusinessOfInterestApp.getInstance().addToRequestQueue(jsonObjReq, requestTag);
    }

    /* This is how to build GET request, in this case you may need to add queries to the url.
     * if you plan to make it POST type add json object instead of null
    */
    public void requestGetJobDetails(String _id) {
        String requestTag = "RequestGetJobDetails";
        String url = Config.SERVER_URL + Config.GET_JOB_DETAILS + _id;

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (response != null && response.has("_id")){
                            JobItem job = JobItem.parseByJsonObject(response);
                            if (job != null){
                                Fragment jobFragment = JobFragment.getInstance();
                                if (!jobFragment.isAdded()){
                                    Bundle bundle = new Bundle();
                                    bundle.putParcelable("parcel_item",job);
                                    jobFragment.setArguments(bundle);


                                    Log.d(TAG, "onResponse: jobfragment sent to eventbus");
                                    EventBus.getDefault().post(jobFragment);
                                }
                            }
                        }
                    }
                }, new APIErrorListener());
        BusinessOfInterestApp.getInstance().addToRequestQueue(jsonObjReq, requestTag);
    }


    public void requestAddNewJobCategory(String categoryJobName, String categoryJobDescription, String filepath) {
        String requestTag = "RequestAddCategoryJob";
        String url = Config.SERVER_URL + Config.ADD_JOB_CATEGORY;
        SimpleMultiPartRequest smr = new SimpleMultiPartRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        requestGetCategories();
                    }
                }, new APIErrorListener());

        smr.addFile("picture", filepath);
        smr.addStringParam("jobCategoryName", categoryJobName);
        smr.addStringParam("jobDescription", categoryJobDescription);
        smr.setShouldCache(false);
        smr.setRetryPolicy(new ApiRetryPolicy(6000,1,1f,smr));
        verifyRequest(smr);
        BusinessOfInterestApp.getInstance().addToRequestQueue(smr, requestTag);
    }

    public void addFacebookUserIfNotExist(JSONObject jsonObject) {
        Log.i(TAG, "addFacebookUserIfNotExist: " + jsonObject.toString());
        try {
            String id = jsonObject.getString("id");
            String name = jsonObject.getString("name");
            String email = jsonObject.getString("email");
            String requestTag = "RequestAddUserByFacebookData";
            String url = Config.SERVER_URL + Config.ADD_FACEBOOK_USER_IF_NOT_EXIST;
            JSONArray params = new JSONArray();
            params.put(id);
            params.put(name);
            params.put(email);
            JsonArrayRequest stringRequest = new JsonArrayRequest(Request.Method.POST, url, params, new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {
                    Log.d(TAG, "onResponse: success, " + response.toString());
                    try {
                        JSONObject userObj = (JSONObject) response.get(0);

                        User user = new User(userObj.getString("_id"), userObj.getString("login"), userObj.getString("email"), userObj.getString("name"));
                        user.setFacebook_id(userObj.getString("facebook_id"));
                        user.setToken(userObj.getString("token"));
                        user.setPassword(userObj.getString("_password"));
						user.setRole(userObj.getString("role"));
                        BusinessOfInterestApp.setLogin(user);
                        SharedPreferences sharedPreferences = context.getSharedPreferences("login", MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("login", user.getLogin());
                        editor.putString("email", user.getEmail());
                        editor.putString("name", user.getName());
                        editor.putString("_password", user.getPassword());
                        editor.putString("_id", user.get_id());
                        editor.putString("facebook_id", user.getFacebook_id());
                        editor.putString("token", user.getToken());
						editor.putString("role", user.getRole());
                        editor.apply();
                        Intent i = new Intent(context, MainActivity.class);
                        if (response.length() == 2) {
                            JSONObject extra = (JSONObject) response.get(1);
                            // if need to check something
                        } else {
                            i.putExtra("new_user", true);
                        }
                        context.startActivity(i);
                        ((Activity) context).finish();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    hideDefaultProgressBar();
                    // recieved response as json array response, now we can send it to its list/place of use
                }
            }, new APIErrorListener());

            showDefaultProgressBar();
            BusinessOfInterestApp.getInstance().addToRequestQueue(stringRequest, requestTag);
        } catch (JSONException e) {
            Toast.makeText(context, "You need to permit the app permission to provide name and email in order to log in", Toast.LENGTH_LONG).show();
        }


    }



    public void showDefaultProgressBar(){
        ProgressPercentage progressEvent = new ProgressPercentage(true,true);
        EventBus.getDefault().post(progressEvent);
    }

    public void hideDefaultProgressBar(){
        ProgressPercentage progressEvent = new ProgressPercentage(true,false);
        EventBus.getDefault().post(progressEvent);
    }


    public void addGoogleUserIfNotExist(String id, String name, String email ) {
        Log.i(TAG, "addGoogleUserIfNotExist");
            String requestTag = "addGoogleUserIfNotExist";
            String url = Config.SERVER_URL + Config.ADD_GOOGLE_USER_IF_NOT_EXIST;
            JSONArray params = new JSONArray();
            params.put(id);
            params.put(name);
            params.put(email);
            JsonArrayRequest stringRequest = new JsonArrayRequest(Request.Method.POST, url, params, new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {
                    Log.d(TAG, "onResponse: success, " + response.toString());
                    try {
                        JSONObject userObj = (JSONObject) response.get(0);

                        User user = new User(userObj.getString("_id"), userObj.getString("login"), userObj.getString("email"), userObj.getString("name"));
                        user.setGoogle_plus_id(userObj.getString("google_plus_id"));
                        user.setToken(userObj.getString("token"));
                        user.setPassword(userObj.getString("_password"));
						user.setRole(userObj.getString("role"));
                        BusinessOfInterestApp.setLogin(user);
                        SharedPreferences sharedPreferences = context.getSharedPreferences("login", MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("login", user.getLogin());
                        editor.putString("email", user.getEmail());
                        editor.putString("name", user.getName());
                        editor.putString("_password", user.getPassword());
                        editor.putString("_id", user.get_id());
                        editor.putString("google_plus_id", user.getGoogle_plus_id());
                        editor.putString("token", user.getToken());
						editor.putString("role", user.getRole());
                        editor.apply();
                        Intent i = new Intent(context, MainActivity.class);
                        if (response.length() == 2) {
                            JSONObject extra = (JSONObject) response.get(1);
                            // if need to check something
                        } else {
                            i.putExtra("new_user", true);
                        }
                        context.startActivity(i);
                        ((Activity) context).finish();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    // recieved response as json array response, now we can send it to its list/place of use
                }
            }, new APIErrorListener());
            BusinessOfInterestApp.getInstance().addToRequestQueue(stringRequest, requestTag);
    }



    public void requestNormalLogin(String login, String password) {
        Log.i(TAG, "requestNormalLogin");

        String requestTag = "requestNormalLogin";
        String url = Config.SERVER_URL + Config.LOGIN;
        JSONArray params = new JSONArray();
        params.put(login);
        params.put(password);
        JsonArrayRequest stringRequest = new JsonArrayRequest(Request.Method.POST, url, params, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Log.d(TAG, "onResponse: success, " + response.toString());
                if (response.length()==1){
                    try {

                        JSONObject userObj = (JSONObject) response.get(0);

                        User user = new User(userObj.getString("_id"), userObj.getString("login"), userObj.getString("email"), userObj.getString("name"));
                        user.setToken(userObj.getString("token"));
                        user.setPassword(userObj.getString("_password"));
						user.setRole(userObj.getString("role"));
                        BusinessOfInterestApp.setLogin(user);
                        SharedPreferences sharedPreferences = context.getSharedPreferences("login", MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("login", user.getLogin());
                        editor.putString("email", user.getEmail());
                        editor.putString("name", user.getName());
                        editor.putString("_password", user.getPassword());
                        editor.putString("_id", user.get_id());
                        editor.putString("token", user.getToken());
						editor.putString("role", user.getRole());
                        editor.apply();
                        Intent i = new Intent(context, MainActivity.class);
                        context.startActivity(i);
                        ((Activity) context).finish();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{
                    Log.d(TAG, "onResponse: success, user info typed are wrong.");
                    EventBus.getDefault().post(new OnFailLogin());
                }
            }
        }, new APIErrorListener());

        BusinessOfInterestApp.getInstance().addToRequestQueue(stringRequest, requestTag);


    }


    public void requestReloginForNewToken(String login, String password, final Request oldRequest) {
        Log.i(TAG, "requestReloginForNewToken");

        String requestTag = "requestReloginForNewToken";
        String url = Config.SERVER_URL + Config.LOGIN_BY_APP;
        JSONArray params = new JSONArray();
        params.put(login);
        params.put(password);
        JsonArrayRequest stringRequest = new JsonArrayRequest(Request.Method.POST, url, params, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Log.d(TAG, "onResponse: success, " + response.toString());
                if (response.length()==1){
                    try {

                        JSONObject userObj = (JSONObject) response.get(0);

                        User user = new User(userObj.getString("_id"), userObj.getString("login"), userObj.getString("email"), userObj.getString("name"));
                        SharedPreferences sharedPreferences = context.getSharedPreferences("login", MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        user.setPassword(userObj.getString("_password"));
                        user.setToken(userObj.getString("token"));
						user.setRole(userObj.getString("role"));
                        if (userObj.has("facebook_id")){
                            user.setFacebook_id(userObj.getString("facebook_id"));
                            editor.putString("facebook_id", user.getFacebook_id());
                        }
                        if (userObj.has("google_plus_id")){
                            user.setGoogle_plus_id(userObj.getString("google_plus_id"));
                            editor.putString("google_plus_id", user.getGoogle_plus_id());
                        }
                        user.setToken(userObj.getString("token"));
                        BusinessOfInterestApp.setLogin(user);

                        editor.putString("login", user.getLogin());
                        editor.putString("email", user.getEmail());
                        editor.putString("_password", user.getPassword());
                        editor.putString("name", user.getName());
                        editor.putString("_id", user.get_id());
                        editor.putString("token", user.getToken());
						editor.putString("role", user.getRole());
                        editor.apply();
                        verifyRequest(oldRequest);
                        BusinessOfInterestApp.getInstance().addToRequestQueue(oldRequest, "requestRetry");

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{
                    Log.d(TAG, "onResponse: success, user info typed are wrong.");
                }
            }
        }, new APIErrorListener());

        BusinessOfInterestApp.getInstance().addToRequestQueue(stringRequest, requestTag);


    }



    private void requestNewToken(Request request){
        if (BusinessOfInterestApp.getLogin() == null){
            Intent i = new Intent(context, LoginActivity.class);
            context.startActivity(i);
            ((Activity)context).finish();
        }else
            requestReloginForNewToken(BusinessOfInterestApp.getLogin().getLogin(), BusinessOfInterestApp.getLogin().getPassword(), request);
    }

    private void verifyRequest(Request request){

        if (BusinessOfInterestApp.getLogin() != null) {
            String credentials = BusinessOfInterestApp.getLogin().getToken();
            String auth = "Basic "
                    + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
            Map<String, String> params = new HashMap<String, String>();
            params.put("Content-Type", "application/json");
            params.put("x-access-token", credentials);
            request.setHeaders(params);
        }
    }

    public void registerNewUser(String login, String password, String email, String name) {

        Log.i(TAG, "registerNewUser");

        String requestTag = "registerNewUser";
        String url = Config.SERVER_URL + Config.SIGNUP;
        JSONArray params = new JSONArray();
        params.put(login);
        params.put(password);
        params.put(email);
        params.put(name);
        JsonArrayRequest stringRequest = new JsonArrayRequest(Request.Method.POST, url, params, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Log.d(TAG, "onResponse: success, " + response.toString());
                if (response.length()==1){
                    try {

                        JSONObject userObj = (JSONObject) response.get(0);

                        User user = new User(userObj.getString("_id"), userObj.getString("login"), userObj.getString("email"), userObj.getString("name"));
                        user.setToken(userObj.getString("token"));
                        user.setPassword(userObj.getString("_password"));
						user.setRole(userObj.getString("role"));
                        BusinessOfInterestApp.setLogin(user);
                        SharedPreferences sharedPreferences = context.getSharedPreferences("login", MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("login", user.getLogin());
                        editor.putString("email", user.getEmail());
                        editor.putString("name", user.getName());
                        editor.putString("_password", user.getPassword());
                        editor.putString("_id", user.get_id());
                        editor.putString("token", user.getToken());
						editor.putString("role", user.getRole());
                        editor.apply();
                        Intent i = new Intent(context, MainActivity.class);
                        i.putExtra("new_user", true);
                        context.startActivity(i);
                        ((Activity) context).finish();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{
                    Log.d(TAG, "onResponse: success, register new user info typed are wrong.");
                }
            }
        }, new APIErrorListener());

        BusinessOfInterestApp.getInstance().addToRequestQueue(stringRequest, requestTag);

    }

    public void requestGetSubCategories(String id) {

        Log.i(TAG, "requestGetSubCategories");

        String requestTag = "requestGetSubCategories";
        String url = Config.SERVER_URL + Config.REQUEST_GET_SUB_CATEGORIES;
        JSONArray params = new JSONArray();
        params.put(id);
        JsonArrayRequest stringRequest = new JsonArrayRequest(Request.Method.POST, url, params, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Log.d(TAG, "onResponse: success, " + response.toString());
                ArrayList<SubCategoryItem> arr = new ArrayList<>();
                for (int i = 0; i < response.length() ; i++) {
                    try {
                        JSONObject obj = response.getJSONObject(i);
                        SubCategoryItem temp = SubCategoryItem.parseJSONObject(obj);
                        if (temp != null) {
                            arr.add(temp);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                EventBus.getDefault().post(new SubCategoryListEvent(arr));
                hideDefaultProgressBar();
            }
        }, new APIErrorListener());
        stringRequest.setRetryPolicy(new ApiRetryPolicy(6000,1,1f,stringRequest));
        verifyRequest(stringRequest);
        showDefaultProgressBar();
        BusinessOfInterestApp.getInstance().addToRequestQueue(stringRequest, requestTag);



    }

    public void requestAddNewSubCategory(String subCategoryName, String subCategoryDescription, String categorySpinner) {

        Log.i(TAG, "requestAddNewSubCategory");

        String requestTag = "requestAddNewSubCategory";
        String url = Config.SERVER_URL + Config.REQUEST_ADD_SUB_CATEGORIES;
        JSONArray params = new JSONArray();
        params.put(subCategoryName);
        params.put(subCategoryDescription);
        params.put(categorySpinner);
        JsonArrayRequest stringRequest = new JsonArrayRequest(Request.Method.POST, url, params, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Toast.makeText(context,"Sub category successfuly added",Toast.LENGTH_LONG).show();
            }
        }, new APIErrorListener());
        stringRequest.setRetryPolicy(new ApiRetryPolicy(6000,1,1f,stringRequest));
        verifyRequest(stringRequest);

        BusinessOfInterestApp.getInstance().addToRequestQueue(stringRequest, requestTag);
    }

    public void requestAddFavoriteJob(String userId, String jobId) {

        Log.i(TAG, "requestAddFavoriteJob");

        String requestTag = "requestAddFavoriteJob";
        String url = Config.SERVER_URL + Config.ADD_FAVORITE;
        JSONArray params = new JSONArray();
        params.put(userId);
        params.put(jobId);
        JsonArrayRequest request = new JsonArrayRequest(Request.Method.POST, url, params, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Toast.makeText(context,"Job was added successfuly to favorites.",Toast.LENGTH_LONG).show();
                hideDefaultProgressBar();
            }
        }, new APIErrorListener());
        request.setRetryPolicy(new ApiRetryPolicy(6000,1,1f,request));
        verifyRequest(request);
        showDefaultProgressBar();
        BusinessOfInterestApp.getInstance().addToRequestQueue(request, requestTag);
    }


    public void requestAddNotification(String jobId,String title, String description, String type) {

        Log.i(TAG, "requestAddNotification");

        String requestTag = "requestAddNotification";
        String url = Config.SERVER_URL + Config.ADD_NOTIFICATION;
        JSONArray params = new JSONArray();
        params.put(jobId);
        params.put(title);
        params.put(description);
        params.put(type);
        JsonArrayRequest request = new JsonArrayRequest(Request.Method.POST, url, params, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Toast.makeText(context,"Notification was added successfuly to job.",Toast.LENGTH_LONG).show();
                hideDefaultProgressBar();
            }
        }, new APIErrorListener());
        request.setRetryPolicy(new ApiRetryPolicy(6000,1,1f,request));
        verifyRequest(request);
        showDefaultProgressBar();
        BusinessOfInterestApp.getInstance().addToRequestQueue(request, requestTag);
    }


    public void requestAddNewJob(String jobName, String jobDescription, String categorySpinner, String subCategorySpinner, String jobImage,String longitude,String latitude,String locationAddress, String userId) {
        String requestTag = "requestAddNewJob";
        String url = Config.SERVER_URL + Config.ADD_NEW_JOB;
        SimpleMultiPartRequest smr = new SimpleMultiPartRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Toast.makeText(context,"Job was added successfully added: "+response,Toast.LENGTH_LONG).show();
                    }
                }, new APIErrorListener());



        smr.setShouldCache(false);
        smr.setRetryPolicy(new ApiRetryPolicy(6000,1,1f,smr));
        verifyRequest(smr);
        smr.addFile("picture", jobImage);
        Log.d(TAG, "requestAddNewJob: jobName: " +jobName);
        smr.addStringParam("jobName", jobName);
        smr.addStringParam("jobDescription", jobDescription);
        smr.addStringParam("categorySpinner", categorySpinner);

        smr.addStringParam("longitude", longitude);
        smr.addStringParam("latitude", latitude);
        smr.addStringParam("locationAddress", locationAddress);


        smr.addStringParam("subCategorySpinner",subCategorySpinner);
        smr.addStringParam("userId",userId);

        smr.setOnProgressListener(new ApiResponseListener(true));
        BusinessOfInterestApp.getInstance().addToRequestQueue(smr, requestTag);

    }

    public void requestGetFavorites(String userId) {

        Log.i(TAG, "requestGetFavorites");

        String requestTag = "requestGetFavorites";
        String url = Config.SERVER_URL + Config.GET_FAVORITES;
        JSONArray params = new JSONArray();
        params.put(userId);
        JsonArrayRequest request = new JsonArrayRequest(Request.Method.POST, url, params, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Toast.makeText(context,"Getting favorites successfuly returned.",Toast.LENGTH_LONG).show();
                hideDefaultProgressBar();
                Tab3DataResponse data = Tab3DataResponse.parseResponse(response);
                if (data.getData().isEmpty()){
                    data.setStatus(1);
                }
                EventBus.getDefault().post(data);
            }
        }, new APIErrorListener(){
            @Override
            public void addErrorHandler(){

                EventBus.getDefault().post(new Tab3DataResponse(2));
            }
        });
        request.setRetryPolicy(new ApiRetryPolicy(6000,1,1f,request));
        verifyRequest(request);
        showDefaultProgressBar();
        EventBus.getDefault().post(new Tab3DataResponse(3));
        BusinessOfInterestApp.getInstance().addToRequestQueue(request, requestTag);
    }


    public class ApiResponseListener implements Response.ProgressListener {
        Boolean hasHisOwnProgressAnimation;

        public ApiResponseListener(Boolean hasHisOwnProgressAnimation) {
            this.hasHisOwnProgressAnimation = hasHisOwnProgressAnimation;
        }


        public Boolean getHasHisOwnProgressAnimation() {
            return hasHisOwnProgressAnimation;
        }

        @Override
        public void onProgress(long transferredBytes, long totalSize) {
            ProgressPercentage progress = new ProgressPercentage();
            if (!getHasHisOwnProgressAnimation()){
                progress.setShowDefaultProgressBar(true);
            }
            Integer percentage = ((Long)(transferredBytes*100/totalSize)).intValue();
            progress.setPercentage(percentage);
            EventBus.getDefault().post(progress);
        }
    }


  /*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */



    /**
     * Default retry policy for requests.
     */
    public class ApiRetryPolicy implements RetryPolicy {
        private Request request
                ;
        /** The current timeout in milliseconds. */
        private int mCurrentTimeoutMs;

        /** The current retry count. */
        private int mCurrentRetryCount;

        /** The maximum number of attempts. */
        private final int mMaxNumRetries;

        /** The backoff multiplier for the policy. */
        private final float mBackoffMultiplier;

        /** The default socket timeout in milliseconds */
        public static final int DEFAULT_TIMEOUT_MS = 30000; //2500

        /** The default number of retries */
        public static final int DEFAULT_MAX_RETRIES = 0;

        /** The default backoff multiplier */
        public static final float DEFAULT_BACKOFF_MULT = 1f;

        /**
         * Constructs a new retry policy using the default timeouts.
         */
        public ApiRetryPolicy() {
            this(DEFAULT_TIMEOUT_MS, DEFAULT_MAX_RETRIES, DEFAULT_BACKOFF_MULT);
        }

        /**
         * Constructs a new retry policy.
         * @param initialTimeoutMs The initial timeout for the policy.
         * @param maxNumRetries The maximum number of retries.
         * @param backoffMultiplier Backoff multiplier for the policy.
         */
        public ApiRetryPolicy(int initialTimeoutMs, int maxNumRetries, float backoffMultiplier) {
            mCurrentTimeoutMs = initialTimeoutMs;
            mMaxNumRetries = maxNumRetries;
            mBackoffMultiplier = backoffMultiplier;
        }

        public ApiRetryPolicy(int initialTimeoutMs, int maxNumRetries, float backoffMultiplier,Request request) {
            mCurrentTimeoutMs = initialTimeoutMs;
            mMaxNumRetries = maxNumRetries;
            mBackoffMultiplier = backoffMultiplier;
            this.request = request;
        }
        /**
         * Returns the current timeout.
         */
        @Override
        public int getCurrentTimeout() {
            return mCurrentTimeoutMs;
        }

        /**
         * Returns the current retry count.
         */
        @Override
        public int getCurrentRetryCount() {
            return mCurrentRetryCount;
        }

        /**
         * Returns the backoff multiplier for the policy.
         */
        public float getBackoffMultiplier() {
            return mBackoffMultiplier;
        }

        /**
         * Prepares for the next retry by applying a backoff to the timeout.
         * @param error The error code of the last attempt.
         */
        @Override
        public void retry(VolleyError error) throws VolleyError {
            mCurrentRetryCount++;
            mCurrentTimeoutMs += (mCurrentTimeoutMs * mBackoffMultiplier);
            Log.d(TAG, "retry loggerpre: "+error.getClass().getSimpleName());
            if (error instanceof AuthFailureError){
                String errorResponse = error.networkResponse.data.toString();
                String errorResponse2 = error.networkResponse.headers.get("error");
                if (errorResponse.equals("expired") || ((errorResponse2 != null) && (errorResponse2.equals("expired")))){
                    requestNewToken(request);
                    throw new ExpiredTokenFailureError();
                }
                if (errorResponse.equals("route is classified for this user.")){
                    throw new NoPermissionTokenFailureError();
                }
                if (errorResponse.equals("no token")){
                    throw new NotProvidedTokenFailureError();
                }
            }
            if (!hasAttemptRemaining()) {
                throw error;
            }
        }

        /**
         * Returns true if this policy has attempts remaining, false otherwise.
         */
        protected boolean hasAttemptRemaining() {
            return mCurrentRetryCount <= mMaxNumRetries;
        }
    }


    private class  ExpiredTokenFailureError extends AuthFailureError{

    }
    private class  NoPermissionTokenFailureError extends AuthFailureError{

    }
    private class  NotProvidedTokenFailureError extends AuthFailureError{

    }



    private class APIErrorListener implements Response.ErrorListener{

        @Override
        public void onErrorResponse(VolleyError error) {
            Log.d(TAG, "onErrorResponse error class: " + error.getClass().getSimpleName());
            if (error instanceof ExpiredTokenFailureError){
                VolleyLog.d(TAG, "Trying to refresh token.");
            }else {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                hideDefaultProgressBar();
                addErrorHandler();
            }
        }

        public void addErrorHandler(){

        }
    }

}
