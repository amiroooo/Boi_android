package com.example.amir.businessofinterest;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.SearchManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.edwardvanraak.materialbarcodescanner.MaterialBarcodeScanner;
import com.edwardvanraak.materialbarcodescanner.MaterialBarcodeScannerBuilder;
import com.example.amir.businessofinterest.Events.BgColorEvent;
import com.example.amir.businessofinterest.Events.CardColorEvent;
import com.example.amir.businessofinterest.Events.FragmentDetach;
import com.example.amir.businessofinterest.Events.ImagePickerEvent;
import com.example.amir.businessofinterest.Events.ProgressPercentage;
import com.example.amir.businessofinterest.Events.TabLayoutVisibility;

import com.facebook.login.LoginManager;
import com.github.ybq.android.spinkit.SpinKitView;

import com.google.android.gms.vision.barcode.Barcode;
import com.mvc.imagepicker.ImagePicker;


import net.glxn.qrgen.android.QRCode;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import eu.long1.spacetablayout.SpaceTab;
import eu.long1.spacetablayout.SpaceTabLayout;


//TODO implement current fragment displayed.
//TODO update deprecated mongoose


public class MainActivity extends AppCompatActivity implements APIRequestHandler, View.OnClickListener {

    public static final String TAG = MainActivity.class.getSimpleName();
    Toolbar toolbar; // toolbar that will have search box and will be added to support actionbar
    private SpaceTabLayout spaceTabLayout; // the tabs layout class
    private List<SpaceTab> spaceTabList; // the specific tab button styling class
    private ViewPager viewPager; // holds the fragments for each chosen tab

    private ApiRequests apiRequests; // will have all url requests and their responses
    private Fragment tab1;
    private boolean isUserLoggedIn = false;
    private boolean isUserAdmin = false;
    private MenuItem logMenu;
    private String currentReplaceFragment;
    private Integer currentTabFragment = 0;
    private ArrayList<CategoryItem> categoryList;
    private ProgressBar progress;
    private SpinKitView defaultInProgress;
    private boolean activityStoped;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_portrait);
        toolbar = findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        viewPager = findViewById(R.id.vp_horizontal_ntb);
        FloatingActionButton floatingButton = findViewById(R.id.floatingActionButton);

        apiRequests = new ApiRequests(this);
        final String[] colors = getResources().getStringArray(R.array.default_preview);
        final String[] colors2 = getResources().getStringArray(R.array.tablayout_colors);


        if (BuildConfig.isLocalDebug) {

        }
        if (BuildConfig.isLocalApi) {
            Config.changeUrlToLocal();
        }

        categoryList = new ArrayList<>();
        setColorsData(Color.parseColor(colors2[0]), Color.parseColor(colors[0]));
        setFragmentsStyle(Color.parseColor(colors2[0]), DataHolder.getStatusBarColor());
        EventBus.getDefault().post(new CardColorEvent(DataHolder.getCardColor()));
        viewPager.setOffscreenPageLimit(5);

        spaceTabLayout = findViewById(R.id.spaceTabLayout);
        progress = findViewById(R.id.progress_bar);
        defaultInProgress = findViewById(R.id.default_in_progress);


        spaceTabList = new ArrayList<>();


        tab1 = new Tab1Fragment();
        Bundle bundleTab1 = new Bundle();
        bundleTab1.putInt("tabId", 1);
        tab1.setArguments(bundleTab1);
        Fragment tab2 = new AbstractTabFragment();
        Bundle bundleTab2 = new Bundle();
        bundleTab2.putInt("tabId", 2);
        tab2.setArguments(bundleTab2);
        Fragment tab3 = new Tab3Fragment();
        Bundle bundleTab3 = new Bundle();
        bundleTab3.putInt("tabId", 3);
        tab3.setArguments(bundleTab3);


        SpaceTab a = new SpaceTab(tab1, getResources().getString(R.string.search_fragment_title), R.drawable.home, Color.parseColor(colors[0]), Color.parseColor(colors2[0]));
        spaceTabList.add(a);
        a = new SpaceTab(tab2, getResources().getString(R.string.queue_fragment_title), R.drawable.queue, Color.parseColor(colors[1]), Color.parseColor(colors2[1]));
        spaceTabList.add(a);
        a = new SpaceTab(tab3, getResources().getString(R.string.favourite_fragment_title), R.drawable.favourite, Color.parseColor(colors[2]), Color.parseColor(colors2[2]));
        spaceTabList.add(a);

        spaceTabLayout.initialize(viewPager, getSupportFragmentManager(), spaceTabList, 0);

        setViewPagerEvents();
        floatingButton.setOnClickListener(this);
        //apiRequests.requestGetCategories();
        Intent search_intent = getIntent();

        SharedPreferences sharedPreferences = getSharedPreferences("login", MODE_PRIVATE);
        verifyLogin(sharedPreferences);

        if (isUserLoggingForFirstTime(search_intent)) {
            runOnlyForFirstTimeLogin();
        } else if (isUserLoggedIn) {
            Toast.makeText(this, "Welcome back " + BusinessOfInterestApp.getLogin().getName(), Toast.LENGTH_LONG).show();
        }
        //ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.}, MY_LOCATION_REQUEST_CODE);

        if (Intent.ACTION_SEARCH.equals(search_intent.getAction())) {
            String query = search_intent.getStringExtra(SearchManager.QUERY);
            try {
                query = URLEncoder.encode(query, "UTF-8");
                // query has sentence written in search box in multilingual.
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

        }

        checkLinkIntent(search_intent);
    }

    private void checkLinkIntent(Intent appLinkIntent){
        if (appLinkIntent == null)
                return;
        Uri appLinkData = appLinkIntent.getData();
        if (appLinkData != null) {
            if (appLinkData.getHost().equals(Config.APP_LINK)) {
                List<String> segments = appLinkData.getPathSegments();
                if(segments.size() == 2){
                    Log.d(TAG, "now switch: "+segments.get(0));
                    String type = segments.get(0);
                    switch (type){
                        case "jobid":{
                            getJobDetailsAndOpen(segments.get(1));
                            break;
                        }
                        case "notifications":{ // probably wont need it
                            Log.d(TAG, "checkLinkIntent: notification: " + segments.get(1));
                            break;
                        }
                    }
                }
            }
        }
    }

    public void getJobDetailsAndOpen(String JobId){
        getApiRequest().requestGetJobDetails(JobId);
    }


    private void verifyLogin(SharedPreferences sharedPreferences) {
        User user = new User();
        if (sharedPreferences.contains("login")) {
            user.setLogin(sharedPreferences.getString("login", null));
        }
        if (sharedPreferences.contains("name")) {
            user.setName(sharedPreferences.getString("name", null));
        }
        if (sharedPreferences.contains("_id")) {
            user.set_id(sharedPreferences.getString("_id", null));
        }
        if (sharedPreferences.contains("email")) {
            user.setEmail(sharedPreferences.getString("email", null));
        }
        if (sharedPreferences.contains("facebook_id")) {
            user.setFacebook_id(sharedPreferences.getString("facebook_id", null));
        }
        if (sharedPreferences.contains("google_plus_id")) {
            user.setGoogle_plus_id(sharedPreferences.getString("google_plus_id", null));
        }
        if (sharedPreferences.contains("token")) {
            user.setToken(sharedPreferences.getString("token", null));
        }
        if (sharedPreferences.contains("_password")) {
            user.setPassword(sharedPreferences.getString("_password", null));
        }
        if (sharedPreferences.contains("role")) {
            user.setRole(sharedPreferences.getString("role", null));
        }		
        if (user.isUserLegal() && user.isUserVefiried()) {
            BusinessOfInterestApp.setLogin(user);
            isUserLoggedIn = true;
            if (user.getRole().equals("admin")){
                isUserAdmin = true;
            }else{
                isUserAdmin = false;
            }
            if (logMenu != null)
                logMenu.setTitle(R.string.logout);
        }
    }

    /*
    * To define and handle the click events for each tab
    */
    private void setViewPagerEvents() {
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(final int position, final float positionOffset, final int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(final int position) {
                spaceTabLayout.show();
                currentReplaceFragment = null;
                currentTabFragment = position;
                // here you can check when user clicked on tab, so add here what to do.
                int bgcolor;
                int buttonColor;
                final String[] colors = getResources().getStringArray(R.array.default_preview);
                final String[] colors2 = getResources().getStringArray(R.array.tablayout_colors);
                switch (position) {
                    case 0: {
                        bgcolor = Color.parseColor(colors2[0]);
                        buttonColor = Color.parseColor(colors[0]);
                        break;
                    }
                    case 1: {
                        bgcolor = Color.parseColor(colors2[1]);
                        buttonColor = Color.parseColor(colors[1]);
                        break;
                    }
                    case 2: {
                        bgcolor = Color.parseColor(colors2[2]);
                        buttonColor = Color.parseColor(colors[2]);
                        break;
                    }
                    case 3: {
                        bgcolor = Color.parseColor(colors2[3]);
                        buttonColor = Color.parseColor(colors[3]);
                        break;
                    }
                    case 4: {
                        bgcolor = Color.parseColor(colors2[4]);
                        buttonColor = Color.parseColor(colors[4]);
                        break;
                    }
                    default: {
                        bgcolor = Color.parseColor(colors2[1]);
                        buttonColor = Color.parseColor(colors[1]);
                        break;
                    }
                }
                int oldBg = DataHolder.getBackgroundColor();
                setColorsData(bgcolor, buttonColor);
                EventBus.getDefault().post(new BgColorEvent(DataHolder.getBackgroundColor(), oldBg));
                EventBus.getDefault().post(new CardColorEvent(DataHolder.getCardColor()));
                setFragmentsStyle(Color.parseColor(colors2[position]), DataHolder.getStatusBarColor());
            }

            @Override
            public void onPageScrollStateChanged(final int state) {

            }
        });

    }

    public ApiRequests getApiRequests() {
        return apiRequests;
    }

    /*
    * changes the background color + tablayout bgcolor + actionbar colors
    */
    public void setColorsData(int bgcolor, int buttonColor) {
        int red = Color.red(bgcolor);
        int green = Color.green(bgcolor);
        int blue = Color.blue(bgcolor);

        red -= 20;
        green -= 20;
        blue -= 20;
        if (red < 0) {
            red = 0;
        }
        if (green < 0) {
            green = 0;
        }
        if (blue < 0) {
            blue = 0;
        }
        int newColor = Color.rgb(red, green, blue);
        int newColorCard = Color.rgb(red + 15, green + 15, blue + 15);

        int newColorStatusBar = Color.rgb((red * 10) / 11, (green * 10) / 11, (blue * 10) / 11);

        DataHolder.setBackgroundColor(newColor);
        DataHolder.setCardColor(newColorCard);
        DataHolder.setStatusBarColor(newColorStatusBar);
    }

    /*
    * To change the statusbar styles (the bar with battery indicator)
    */
    private void setFragmentsStyle(int bgColor, int status_bar_color) {
        toolbar.setBackgroundColor(bgColor);
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(status_bar_color);
        }
    }

    @SuppressWarnings("UnusedParameters")
    public boolean openPopupMenu(MenuItem item) {
        return true;
    }

    @SuppressWarnings("UnusedParameters")
    public boolean onClickLoginButton(MenuItem item) {
        if (isUserLoggedIn){
            isUserLoggedIn = false;
            BusinessOfInterestApp.setLogin(null);
            SharedPreferences sharedPreferences = getSharedPreferences("login", MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.clear();
            editor.apply();
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(R.string.dialog_loggedout_message).setTitle(R.string.dialog_loggedout_title);
            AlertDialog dialog = builder.create();
            dialog.show();
            item.setTitle(R.string.action_sign_in);
            return true;
        }else {
            Intent i = new Intent(getBaseContext(), LoginActivity.class);
            startActivity(i);
            finish();
            LoginManager.getInstance().logOut();
            return true;
        }
    }

    private boolean isUserLoggingForFirstTime(Intent i){
        if(i.getBooleanExtra("new_user",false)) {
            return true;
        }else
            return false;
    }

    private void runOnlyForFirstTimeLogin() {
        Toast.makeText(this, "Welcome to Business of Interest " + BusinessOfInterestApp.getLogin().getName(), Toast.LENGTH_LONG).show();

    }

    /*
    * Inflating the searchbox
    */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        SearchView search_view = (SearchView) menu.findItem(R.id.menu_search).getActionView();
        SearchManager searchManager = (SearchManager)getSystemService(SEARCH_SERVICE);
        search_view.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        search_view.setBackgroundColor(Color.WHITE);

        logMenu = menu.getItem(1);

        if (isUserLoggedIn)
            logMenu.setTitle(R.string.logout);

        if (Build.VERSION.SDK_INT < 21) {
            search_view.setBackgroundColor(DataHolder.getBackgroundColor());
        }
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Bitmap bitmap = ImagePicker.getImageFromResult(this, requestCode, resultCode, data);
        Log.i(TAG,"picture available on main activity");
        // TODO do something with the bitmap
        if (bitmap != null){
            bitmap = editBitmapIfNeeded(bitmap);
            File file = saveBitmapTemporarily(bitmap);
            EventBus.getDefault().post(new ImagePickerEvent(file.getAbsolutePath()));

            //file.delete();

        }
    }

    private Bitmap editBitmapIfNeeded(Bitmap bitmap) {
        // resize image if needed in some cases for example we can check which fragment or tab is on and based on that we can decide if image need resizing.
        return bitmap;
    }

    public File saveBitmapTemporarily(Bitmap bitmap){
        File f3=new File(Environment.getExternalStorageDirectory() + "/tempimg");
        if(!f3.exists())
            f3.mkdirs();
        OutputStream outStream = null;
        File file = new File(Environment.getExternalStorageDirectory() + "/tempimg/file.png");
        if(!file.exists())
            try {
                file.createNewFile();
            } catch (IOException e) {
                return null;
            }
        try {
            outStream = new FileOutputStream(file,false);
            bitmap.compress(Bitmap.CompressFormat.PNG, 85, outStream);
            outStream.close();
            return file;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }



    public void pickImage() {
        // Click on image button
        ImagePicker.pickImage(this, "Select your image:");
    }

    @Override
    protected void onDestroy() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        super.onDestroy();
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()) {
            case R.id.floatingActionButton:{
                switch (getCurrentFragment()){
                    case "tab0":{ // home tab
                        if (isUserAdmin){
                            AddNewCategoryFragment frag = new AddNewCategoryFragment();
                            Bundle bundle = new Bundle();
                            bundle.putString("title","New Category Creator");
                            frag.setArguments(bundle);
                            showChosenFragment(frag);
                        }


                        //apiRequests.requestAddNotification("5a2726fee96bfb2b40563281","Welcome","Welcome everyone to my barber shop notification page!<br>I hope that you find this page helpful.","soft");
                        //apiRequests.requestAddNotification("5a2726fee96bfb2b40563281","NOTE","Shop is not Open Tommorow.","grip");
                        //apiRequests.requestAddNotification("5a2726fee96bfb2b40563281","Discount","For next week, each person gets to have his hair cut with 25% less money.<br>Hurry up and come!.","hard");

                        break;
                    }
                    case "tab1":{ // queue tab
                        /*
                        if (isUserAdmin){
                            AddNewSubCategoryFragment frag = new AddNewSubCategoryFragment();
                            Bundle bundle = new Bundle();
                            bundle.putString("title","New Sub Category Creator");
                            frag.setArguments(bundle);
                            showChosenFragment(frag);
                        }
                        */
                        //apiRequests.requestAddFavoriteJob(BusinessOfInterestApp.getLogin().get_id(),"5a2726fee96bfb2b40563281");
                        TestFragment frag = new TestFragment();
                        showChosenFragment(frag);

                        break;
                    }
                    case "tab2":{ // favourite tab
                        if (isUserAdmin){
                            AddNewJobFragment frag = new AddNewJobFragment();
                            Bundle bundle = new Bundle();
                            bundle.putString("title","New Job Creator");
                            frag.setArguments(bundle);
                            showChosenFragment(frag);
                        }
                        break;
                    }
                    case "JobsInListFragment":{

                        break;
                    }
                }
                Log.d(TAG, "onClick: "+getCurrentFragment());
                //apiRequests.requestRegisterNewJob("Zahra Dentistry", "5a1a06443e53ce2b3869331b", "Best dentistry in world near meuhedet.");
                //pickImage();
                break;
            }
        }

    }

    @Override
    public void onStart() {
        super.onStart();

        if (!EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().register(this);
        }
        activityStoped = false;
    }

    @Override
    public void onStop() {
        super.onStop();
        activityStoped = true;
        //if (EventBus.getDefault().isRegistered(this)){
        //    EventBus.getDefault().unregister(this);
        //}
    }


    private class TestAsync extends AsyncTask<Fragment, Integer, Fragment>
    {
        String TAG = getClass().getSimpleName();

        @Override
        protected Fragment doInBackground(Fragment... fragments) {
            while(activityStoped){
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            return fragments[0];
        }

        @Override
        protected void onPostExecute(Fragment fragment) {
            super.onPostExecute(fragment);
            showChosenFragment(fragment);
        }
    }



    @Subscribe(threadMode = ThreadMode.MAIN)
    public void showChosenFragment(Fragment fragment) {
        if (activityStoped) {
            TestAsync t = new TestAsync();
            t.execute(fragment);
        }else {
            if (!fragment.isAdded()) {
                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.setCustomAnimations(R.anim.slide_from_right_to_left, R.anim.slide_to_exit_left, R.anim.slide_from_right_to_left, R.anim.slide_to_exit_left);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.add(R.id.container, fragment);


                fragmentTransaction.commit();
                setCurrentReplaceFragment(fragment.getClass().getSimpleName());
            }
        }
    }

    private Bitmap generateQR(String text) {
        return QRCode.from(text).withSize(Utils.dpToPx(250), Utils.dpToPx(250)).bitmap();
    }

    public Bitmap generateNotificationQR(String jobId){
        return generateQR(Config.SERVER_URL+Config.GET_NOTIFICATION_PAGE + jobId );
    }

    public Bitmap generateJobQR(String jobId){
        return generateQR(Config.APP_LINK+Config.APP_LINK_JOBID + jobId );
    }
    public void startScan(MenuItem item) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.popBackStack();
        final MaterialBarcodeScanner materialBarcodeScanner = new MaterialBarcodeScannerBuilder()
                .withActivity(this)
                .withEnableAutoFocus(true)
                .withBleepEnabled(true)
                .withBackfacingCamera()
                .withCenterTracker()
                .withText("Scanning...")
                .withResultListener(new MaterialBarcodeScanner.OnResultListener() {
                    @Override
                    public void onResult(Barcode barcode) {
                        Log.d(TAG, "onResult: " + barcode.rawValue);

                        List<String> list = new ArrayList<String>(Arrays.asList(barcode.rawValue.split("/")));
                        Log.d(TAG, "onResult: " +list.toString());
                        if (list.get(1).equals("jobid")) {
                            getJobDetailsAndOpen(list.get(2));
                        }
                    }
                })
                .build();
        materialBarcodeScanner.startScan();
        //return true;
    }

    // returns fragment name that is on in order to adapt the floating button action to what to do.
    public String getCurrentFragment(){

        if (currentReplaceFragment != null){
            return currentReplaceFragment;
        }
        else
            return "tab" + currentTabFragment.toString();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void clearCurrentReplaceFragment(FragmentDetach event) {
        this.currentReplaceFragment = null;
    }
    public void setCurrentReplaceFragment(String fragName) {
        this.currentReplaceFragment = fragName;
    }




    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onProgress(ProgressPercentage event) {
        if (event.getShowDefaultProgressBar()){
            updateDefaultProgressBar(event.getVisibility());
        }else
            updateProgressBar(event.getPercentage());

    }

    private void updateDefaultProgressBar(Boolean visibility) {
        if (visibility){
            defaultInProgress.setVisibility(View.VISIBLE);
        }else{
            defaultInProgress.setVisibility(View.INVISIBLE);
        }
    }

    private void updateProgressBar(Integer percentage) {
        if (percentage >= 0){
            progress.setVisibility(View.VISIBLE);
        }
        if (percentage >= 99){
            progress.setVisibility(View.INVISIBLE);
        }
        progress.setProgress(percentage);
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void tabLayoutVisibility(TabLayoutVisibility event){
        if (event.getShouldShow()){
            spaceTabLayout.show();
        }else
            spaceTabLayout.hide();
    }

    @Override
    public ApiRequests getApiRequest() {
        return apiRequests;
    }

    public void setCategoryList(ArrayList<CategoryItem> categoryList) {
        this.categoryList = categoryList;
    }

    public ArrayList<CategoryItem> getCategoryList() {
        return categoryList;
    }
}