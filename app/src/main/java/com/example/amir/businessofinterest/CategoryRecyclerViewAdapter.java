package com.example.amir.businessofinterest;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.example.amir.businessofinterest.Events.Tab1DataResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Amir on 11/24/2017.
 */

public class CategoryRecyclerViewAdapter extends RecyclerView.Adapter<CategoryRecyclerViewAdapter.CategoryViewHolder> {

        private List<CategoryItem> mData;
        private LayoutInflater mInflater;
        private ItemClickListener mClickListener;
        private Context context;

        // data is passed into the constructor
        CategoryRecyclerViewAdapter(Context context) {
            this.context = context;
            this.mInflater = LayoutInflater.from(context);
            this.mData = new ArrayList<CategoryItem>();
        }

        // inflates the cell layout from xml when needed
        @Override
        public CategoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = mInflater.inflate(R.layout.category_item, parent, false);
            return new CategoryViewHolder(view);
        }

        // binds the data to the textview in each cell
        @Override
        public void onBindViewHolder(CategoryViewHolder holder, int position) {
            CategoryItem categoryItem = mData.get(position);
            holder.updateCategoryHolderWithCategoryItem(categoryItem);
        }

        // total number of cells
        @Override
        public int getItemCount() {
            if (mData == null){
                return 0;
            }
            return mData.size();
        }

    public void updateData(Tab1DataResponse event) {
        mData = event.getData();
        notifyDataSetChanged();
    }


    // stores and recycles views as they are scrolled off screen
        public class CategoryViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            CategoryItem categoryItem;
            TextView categoryNameTextView;
            TextView categoryDescriptionTextView;
            ImageView categoryPicture;

            CategoryViewHolder(View itemView) {
                super(itemView);

                itemView.setOnClickListener(this);
            }

            @Override
            public void onClick(View view) {
                if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
            }

            public void updateCategoryHolderWithCategoryItem(CategoryItem categoryItem) {
                this.categoryItem = categoryItem;
                categoryNameTextView = itemView.findViewById(R.id.category_name);
                categoryDescriptionTextView = itemView.findViewById(R.id.category_description);
                categoryPicture = itemView.findViewById(R.id.category_picture);
                categoryNameTextView.setText(categoryItem.getCategoryName());
                categoryDescriptionTextView.setText(categoryItem.getCategoryDescription());

                RequestOptions myOptions = new RequestOptions().placeholder(R.mipmap.overlay).centerCrop();
                Glide.with(itemView.getContext())
                        .load(categoryItem.getCategoryPictureUrl())
                        .apply(myOptions)
                        .into(categoryPicture); //


                final String[] colors = context.getResources().getStringArray(R.array.default_preview);
                CardView card = itemView.findViewById(R.id.category_card);
                card.setCardBackgroundColor(Color.argb(100,229, 209, 209));
            }
        }

        // convenience method for getting data at click position
        CategoryItem getItem(int index) {
            return mData.get(index);
        }

        // allows clicks events to be caught
        void setClickListener(ItemClickListener itemClickListener) {
            this.mClickListener = itemClickListener;
        }

        // parent activity will implement this method to respond to click events
        public interface ItemClickListener {
            void onItemClick(View view, int position);
        }



}
