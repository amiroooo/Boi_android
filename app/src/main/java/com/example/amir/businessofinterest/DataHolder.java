package com.example.amir.businessofinterest;

/**
 * Created by Amir on 11/23/2017.
 */

public class DataHolder {

    private static int backgroundColor = 15738396;
    private static int cardColor = 15738396;
    private static int statusBarColor;




    public static int getBackgroundColor() {
        return backgroundColor;
    }

    public static void setBackgroundColor(int backgroundColor) {
        DataHolder.backgroundColor = backgroundColor;
    }


    public static int getCardColor() {
        return cardColor;
    }

    public static void setCardColor(int cardColor) {
        DataHolder.cardColor = cardColor;
    }

    public static void setStatusBarColor(int statusBarColor) {
        DataHolder.statusBarColor = statusBarColor;
    }
    public static int getStatusBarColor() {
        return statusBarColor;
    }


}
